<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="/img/favicon.png">

    <title>CaroCon</title>

    <link href="css/bootstrap.css" rel="stylesheet" media="screen">
    <link href="css/styles.css" rel="stylesheet" media="screen">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,400italic,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="../../assets/js/html5shiv.js"></script>
      <script src="../../assets/js/respond.min.js"></script>
    <![endif]-->

  </head>

  <body>

      <!-- Fixed navbar -->
    <div class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/index.php"><img class="img-responsive" src="img/carocon-logo.png" alt="Carocon Logo"></a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li><a href="/packaging.php">Packaging</a></li>
            <li><a href="/displays.php">Displays</a></li>
            <li><a href="/about-us.php">About Us</a></li>
          </ul>
          <div class="nav navbar-right">
            <a data-toggle="modal" href="#myModal" class="btn btn-default">Contact Us</a>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>

<!--START MODAL -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-7 modal-contact">
              <h2>We are here to help:</h2>
              <p>We Provide custom solutions. Let's get started!</p>
              <div><input type="text" class="form-control row-contact-input" placeholder="Full Name"></div>
              <div><input type="text" class="form-control row-contact-input" placeholder="Phone Number"></div>
              <div><input type="text" class="form-control row-contact-input" placeholder="Email Address"></div>
              <div class="pull-right"><button class="btn btn-primary row-contact-btn" type="button">Get Started</button></div>
            </div>
            <div class="col-sm-5 modal-contact-2">
              <h3>By Phone:</h3>
              <p>1.800.586.1258</p>
              <h3>By Email:</h3>
              <p><a href="mailto:info@caroconusa.com">info@caroconusa.com</a></p>
              <h3>By Mail:</h3>
              <p>123 Fake Street<br>Anytown USA 12345</p>
              <h3>Get to Know Us Better:</h3>
                <div class="social-icons">
                <img src="img/facebook-icon.png" alt="Facebook">
                <img src="img/twitter-icon.png" alt="Twitter">
                <img src="img/youtube-icon.png" alt="YouTube">
                </div>                          
            </div>
          </div>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
<!--END MODAL-->

      <div class="main-bg"><div class="row row-top">
        <div class="container">
          <div class="col-sm-7">
            <div class="row-top-title">Get your product in front of the public with speed and style.</div>
            <div class="row-top-text">We design, manufacture and distribute attention-grabbing packages for some of the world’s biggest brands. (But we’ve got a soft spot for brands that are just being born.) We do it fast, and with flair.</div>
            <a href="#howitworks"><button class="btn btn-primary row-top-btn" type="button">How It Works</button></a>
          </div>
          <div class="col-sm-5 hidden-xs"></div>
        </div>
      </div> </div>

      <div class="row row-process"><a name="howitworks"></a>
        <div class="container">
          <div class="col-sm-12">
            <div class="row-process-title"><h5>Our Process</h5></div>
            <h3>A great product deserves a great presentation.</h3>
            <div class="row-process-text">And it needs it, like, yesterday.</div>
          </div>
        </div>
      </div> 

      <div class="row row-color">
        <div class="container">
          <div class="col-sm-7">
            <h2>Heading</h2>
            <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
          </div>
          <div class="col-sm-5"></div>
        </div>
      </div>

      <div class="row row-white">
        <div class="container">
          <div class="col-sm-5"></div>
          <div class="col-sm-7">
            <h2>Heading</h2>
            <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
          </div>
        </div>
      </div>

      <div class="row row-color">
        <div class="container">
          <div class="col-sm-7">
            <h2>Heading</h2>
            <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
          </div>
          <div class="col-sm-5"></div>
        </div>
      </div>

      <div class="row row-white">
        <div class="container">
          <div class="col-sm-5"></div>
          <div class="col-sm-7">
            <h2>Heading</h2>
            <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
          </div>
        </div>
      </div>

      <div class="row-contact-bg">
      <div class="row row-contact">
        <div class="container">
          <div class="col-xs-12">
            <h1 class="row-contact-title">We can do the same for you. Call today and let's get started.</h1>
          </div>
        </div>
        <div class="container">
          <div class="col-sm-3"><input type="text" class="form-control row-contact-input" placeholder="Full Name"></div>
          <div class="col-sm-3"><input type="text" class="form-control row-contact-input" placeholder="Phone Number"></div>
          <div class="col-sm-3"><input type="text" class="form-control row-contact-input" placeholder="Email Address"></div>
          <div class="col-sm-3"><button class="btn btn-primary row-contact-btn" type="button">Get Started</button></div>
        </div>
      </div>   
      </div>  

      <div class="row row-learn-more">
        <div class="container">
          <div class="col-sm-6">
            <h5>Learn More About</h5>
            <button class="btn btn-default row-learn-more-btn" type="button">Packaging</button>
          </div>
          <div class="col-sm-6">
            <h5>Learn More About</h5>
            <button class="btn btn-default row-learn-more-btn" type="button">Displays</button>
          </div>
        </div>
      </div> 

      <footer>
        <div class="container footer-content">
          <div class="col-sm-4"><img class="img-circle img-responsive" src="http://dummyimage.com/200x200/282828/fff.jpg" alt="Footer Image"></div>
          <div class="col-sm-4">
            <h6>Packaging & Design Solutions</h6>
            <ul class="footer-links">
              <li><a href="/solutions.php#assembly">Assembly & Fulfillment</a></li>
              <li><a href="/solutions.php#contract">Contract Packaging</a></li>
              <li><a href="/solutions.php#point">Point-of-Purchase Display</a></li>
              <li><a href="/solutions.php#technology">Technology</a></li>
              <li><a href="/solutions.php#production">Production</a></li>
              <li><a href="/solutions.php#development">Development</a></li>
              <li><a href="/solutions.php#prototyping">Prototyping</a></li>
              <li><a href="/solutions.php#project">Project Management</a></li>
              <li><a href="/solutions.php#design">Design</a></li>
            </ul>
          </div>
          <div class="col-sm-4">
            <h6>Contact Us</h6>
            <p class="phone">1-800-895-5959</p>
            <p class="email"><a href="mailto:info@caroconusa.com">info@caroconusa.com</a></p>
            <div class="social-icons">
              <img src="img/facebook-icon.png" alt="Facebook">
              <img src="img/twitter-icon.png" alt="Twitter">
              <img src="img/youtube-icon.png" alt="YouTube">
            </div>
          </div>
        </div>
        <div class="container">
          <div class="copyright col-xs-12">CaroCon Copyright &copy; 2013 Privacy Policy</div>
        </div>
      </footer>

    <script src="https://code.jquery.com/jquery.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/myscripts.js"></script>
  </body>
</html>